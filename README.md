# Necessary Mod Pack

The most necessary mods. List:

* **Essentials** - more commands (spawn, warps, homes, mutes, bans, vanish and etc)
* **Bug Report** - GUI form for sending information to log
* **PVP Control** - protect newbies and other
* **Superchat** - chat channels and prefixes, filters
* **Stats** - server online statistic
* **Query** - easy socket server for sending server info
* **Coins** - easy money
* **Group Perms** - manage privs in player groups

Read all readme's in mod folders for more information

## Install depends

### Mod depends

* gui_menu
* mod_configs
* cuboids_lib

From **code_libs**.

### Lua depends

This modpack require librays from [luarocks](https://luarocks.org/). For using **query** need disable mod security.

```
# for query
luarocks install effil
pacman -S lua51-socket
```
